// Convertir une date en heure lisible par un humain, UTC
// v1.0

module.exports = function(date, string, lang) {
    if (string.indexOf('$n') != -1) {
        var daysEN = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        var daysFR = new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");

        if (lang == 'fr') {
            string = string.replace('$n', `${daysFR[date.getDay()]}`)
        } else {
            string = string.replace('$n', `${daysEN[date.getDay()]}`)
        }
    }

    if (string.indexOf('$d') != -1) {
        string = string.replace('$d', date.getDate())
    }

    if (string.indexOf('$o') != -1) {
        var monthsEN = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var monthsFR = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        if (lang == 'fr') {
            string = string.replace('$o', monthsFR[date.getMonth()])
        } else {
            string = string.replace('$o', monthsEN[date.getMonth()])
        }
    }

    if (string.indexOf('$y') != -1) {
        string = string.replace('$y', date.getFullYear())
    }
    
    if (string.indexOf('$h') != -1) {
        var stringAdd = '';
        if (date.getUTCHours().toString().length == 1) {
            stringAdd += '0';
        }
        if (lang == 'fr') {
            stringAdd += date.getUTCHours()+1;
        } else {
            stringAdd += date.getUTCHours();
        }
        string = string.replace('$h', stringAdd);
    }
    
    if (string.indexOf('$m') != -1) {
        var stringAdd = '';
        if (date.getUTCMinutes().toString().length == 1) {
            stringAdd += '0';
        }
        stringAdd += date.getUTCMinutes();
        string = string.replace('$m', stringAdd);
    }
    
    if (string.indexOf('$s') != -1) {
        var stringAdd = '';
        if (date.getUTCSeconds().toString().length == 1) {
            stringAdd += '0';
        }
        stringAdd += date.getUTCSeconds();
        string = string.replace('$s', stringAdd);
    }
    
    return string;
}