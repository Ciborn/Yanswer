const poolQuery = require('./../functions/database/poolQuery');
const replaceDiacritics = require('./../functions/replaceDiacritics');
module.exports = function(bot, message) {
    const filter = m => m.author.id == message.author.id;
    var trigger = null;
    var answer = null;
    var recipient = null;
    var validated = 0;
    
    const verifiedContributors = `267677320712683523 320933389513523220 205289297170006018`;
    if (verifiedContributors.indexOf(message.author.id) != -1) {
        validated = 1;
    }
    message.channel.send(`Tu veux ajouter quelque chose dans ma base de connaissances ? Avec plaisir !\nCommençons, pour quelle phrase je serais déclenché ?`);
    message.channel.awaitMessages(filter, { max: 1, time: 60000, errors: ['time']}).then(function(collected) {
        message.channel.send(`Bien, et qu'est-ce que tu veux que je réponde à **${collected.first().content}** ?`);
        trigger = new String(replaceDiacritics(collected.first().content)).toLowerCase();
        message.channel.awaitMessages(filter, { max: 1, time: 60000, errors: ['time']}).then(function(collected) {
            message.channel.send(`Parfait, je répondrai donc **${collected.first().content}**. Et veux-tu qu'il soit adressé à quelqu'un de spécifique ? (Si tu veux qu'il soit adressé à tout le monde, entre *everyone*, sinon, entre son ID Discord)`);
            answer = collected.first().content;
            message.channel.awaitMessages(filter, {max: 1, time: 60000, errors: ['time']}).then(function(collected) {
                message.channel.send(`Ce message sera donc adressé à **${collected.first().content}**. A présent, j'ai besoin de finaliser quelques petits trucs, laisse-moi un peu de temps.`);
                recipient = collected.first().content;
                poolQuery(`INSERT INTO data (question, answer, recipient, language, code, author, validated, createdTimestamp) VALUES (
                    "${trigger}", "${answer}", '${recipient}', 'fr', 'null', '${message.author.id}', ${validated}, ${new Date().getTime()}
                )`, function(err, result) {
                    if (err) {
                        message.channel.send(`Une erreur est survenue.\n${err}`);
                    } else {
                        if (validated == 0) {
                            message.channel.send(`Très bien, j'ai tout enregistré. Il faudra attendre que ton message soit validé pour qu'il soit actif sur moi. Aussi, sache que si tu veux faire une quelconque modification plus tard, l'ID de la correspondance est **${result.insertId}**.`);
                        } else {
                            if (message.author.id == '320933389513523220') {
                                message.channel.send(`Tout a été enregistré avec l'ID **${result.insertId}**.`);
                            } else {
                                message.channel.send(`Très bien, j'ai tout enregistré. Ton message a été automatiquement approuvé car tu es dans la liste des contributeurs vérifiés. Aussi, sache que si tu veux faire une quelconque modification plus tard, l'ID de la correspondance est **${result.insertId}**.`);
                            }
                        }
                    }
                })
            })
        });
    });
}