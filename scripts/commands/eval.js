// eval

const Discord = require('discord.js');
module.exports = function(bot, message) {
    if (message.author.id == '320933389513523220') {
        try {
            const messageContentLengthReduced = message.content.length - 6;
            const code = message.content.substr(6, messageContentLengthReduced);
            let evaled = eval(code);
            
            if (typeof evaled !== "string") {
                evaled = require("util").inspect(evaled);
            }
            
            message.channel.send(evaled, {code:"xl"});
        } catch (err) {
            message.channel.send(`Une erreur est survenue.\n\`\`\`xl\n${err}\n\`\`\``);
        }
    } else {
        message.channel.send(`:red_circle:  Désolé, tu n'es pas assez puissant pour faire ça.`);
    }
}