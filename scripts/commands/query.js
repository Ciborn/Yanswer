// query

const util = require('util');
module.exports = function(bot, message) {
    if (message.author.id == '320933389513523220') {
        try {
            messageContentLengthReduced = message.content.length - 7;
            const result = require('./../functions/database/poolQuery.js')(message.content.substr(7, messageContentLengthReduced), function(err, result) {
                if (err) {
                    message.channel.send(`Une erreur est survenue.\n\`\`\`xl\n${err}\n\`\`\``);
                } else {
                    message.channel.send(util.inspect(result, false, null), {code:"xl"});
                }
            });
        } catch (err) {
            message.channel.send(`Une erreur est survenue.\n\`\`\`xl\n${err}\n\`\`\``);
        }
    } else {
        message.channel.send(`:red_circle:  Non non, bien essayé, mais tu ne t'appelles pas Ciborn.`);
    }
}