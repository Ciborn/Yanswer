const poolQuery = require('./../functions/database/poolQuery');
const replaceDiacritics = require('./../functions/replaceDiacritics');
module.exports = function(bot, message, callback) {
    callback("success");
    poolQuery(`SELECT * FROM data WHERE validated=1`, function(err, result) {
        if (err) {
            callback('mysqlError', null, err);
        } else {
            const filter = m => m.author.id == message.author.id;
            var allResults = new Array();
            var correspondingRecipient = new Array();
            var oneResultFound = new Boolean(false);
            var specificRecipientFound = new Boolean(false);
            for (var n = 0; n < Object.keys(result).length; n++) {
                if (new String(replaceDiacritics(message.content)).toLowerCase().indexOf(result[n].question) != -1) {
                    oneResultFound = true;
                    if (result[n].recipient == message.author.id) {
                        specificRecipientFound = true;
                        correspondingRecipient.push(result[n]);
                    } else {
                        allResults.push(result[n]);
                    }
                }
            }

            const specificAnswer = correspondingRecipient[Math.floor(Math.random() * correspondingRecipient.length)];
            const everyoneAnswer = allResults[Math.floor(Math.random() * allResults.length)];
            if (oneResultFound == true) {
                if (specificRecipientFound == true) {
                    message.channel.send(specificAnswer.answer);
                    message.channel.awaitMessages(filter, {max: 1, time: 30000, errors: ['time']}).then(collected => {
                        if (collected.first().content.indexOf('info') != -1) {
                            require('./info')(bot, message, specificAnswer.id);
                        }
                    });
                } else {
                    message.channel.send(everyoneAnswer.answer);
                    message.channel.awaitMessages(filter, {max: 1, time: 30000, errors: ['time']}).then(collected => {
                        if (collected.first().content.indexOf('info') != -1) {
                            require('./info')(bot, message, everyoneAnswer.id);
                        }
                    });
                }
            }
        }
    })
}