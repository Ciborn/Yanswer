module.exports = function(message, bot, callback) {
    const ping = new Date().getTime();
    message.channel.send(`Octroie-moi quelques centaines de millisecondes, je fais quelques calculs...`).then(newMessage => {
        newMessage.edit(`Le WebSocket Heartbeat actuel est de ${Math.floor(bot.ping)} ms, et le ping général du bot est de ${new Date().getTime() - ping} ms.`);
    });
    callback('success');
}