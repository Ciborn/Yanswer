const poolQuery = require('./../functions/database/poolQuery');
const convertDate = require('./../functions/utils/convertDate');
module.exports = function(bot, message, id) {
    poolQuery(`SELECT * FROM data WHERE id=${id}`, function(err, result) {
        if (err) {
            message.channel.send(`Pour une raison inconnue (pas inconnue mais j'ai juste la flemme de le dire), je n'ai pas pu t'afficher les informations sur cette correspondance.`);
        } else {
            var author = '';
            var recipient = '';
            var validated = '';
            
            if (result[0].validated == 0) {
                validated = `n'a pas encore été validé`;
            } else {
                validated = `a été validé`;
            }

            if (result[0].author == message.author.id) {
                if (result[0].recipient == message.author.id) {
                    message.channel.send(`La correspondance **${id}** déclenché avec **${result[0].question}** répondant par **${result[0].answer}** a été créé par **toi-même** et est destiné à **toi-même**. Il ${validated} et a été créé le **${convertDate(new Date(result[0].createdTimestamp))}**.`);
                } else if (result[0].recipient == 'everyone') {
                    message.channel.send(`La correspondance **${id}** déclenché avec **${result[0].question}** répondant par **${result[0].answer}** a été créé par **toi-même** et est destiné à **tout le monde**. Il ${validated} et a été créé le **${convertDate(new Date(result[0].createdTimestamp))}**.`);
                } else {
                    bot.fetchUser(result[0].recipient).then(user => {
                        message.channel.send(`La correspondance **${id}** déclenché avec **${result[0].question}** répondant par **${result[0].answer}** a été créé par **toi-même** et est destiné à **${user.username}**. Il ${validated} et a été créé le **${convertDate(new Date(result[0].createdTimestamp))}**.`);
                    })
                }
            } else {
                bot.fetchUser(result[0].author).then(user => {
                    if (result[0].recipient == message.author.id) {
                        message.channel.send(`La correspondance **${id}** déclenché avec **${result[0].question}** répondant par **${result[0].answer}** a été créé par **${user.username}** et est destiné à **toi-même**. Il ${validated} et a été créé le **${convertDate(new Date(result[0].createdTimestamp))}**.`);
                    } else if (result[0].recipient == 'everyone') {
                        message.channel.send(`La correspondance **${id}** déclenché avec **${result[0].question}** répondant par **${result[0].answer}** a été créé par **${user.username}** et est destiné à **tout le monde**. Il ${validated} et a été créé le **${convertDate(new Date(result[0].createdTimestamp))}**.`);
                    } else {
                        bot.fetchUser(result[0].recipient).then(recipient => {
                            message.channel.send(`La correspondance **${id}** déclenché avec **${result[0].question}** répondant par **${result[0].answer}** a été créé par **${user.username}** et est destiné à **${recipient.username}**. Il ${validated} et a été créé le **${convertDate(new Date(result[0].createdTimestamp))}**.`);
                        })
                    }
                })
            }
        }
    })
}