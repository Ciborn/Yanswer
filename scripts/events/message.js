const poolQuery = require('./../functions/database/poolQuery');
module.exports = function(message, bot, guildPrefixes, guildSurnames, callback) {
    if (guildPrefixes.has(message.guild.id)) {
        if (message.content.indexOf(guildPrefixes.get(message.guild.id)) == 0) {
            const prefix = guildPrefixes.get(message.guild.id);
            const args = message.content.slice(prefix.length).trim().split(/ +/g);
            const command = args.shift().toLowerCase();
            if (message.content.indexOf(`${prefix}ping`) == 0) {
                require('./../commands/ping')(message, bot, function(state) {
                    callback(state);
                });
            } else if (message.content.indexOf(`${prefix}eval`) == 0) {
                require('./../commands/eval')(bot, message);
            } else if (message.content.indexOf(`${prefix}query`) == 0) {
                require('./../commands/query')(bot, message);
            } else if (message.content.indexOf(`${prefix}teach`) == 0) {
                require('./../commands/teach')(bot, message);
            } else if (message.content.indexOf(`${prefix}info`) == 0) {
                if (message.content.length != prefix.length + 4) {
                    require('./../commands/info')(bot, message, args[0]);
                }
            }
        } else {
            if (guildSurnames.has(message.guild.id)) {
                if (message.content.indexOf(guildSurnames.get(message.guild.id)) == 0) {
                    require('./../commands/answering')(bot, message, function(state, details, message) {
                        callback(state, details, message);
                    });
                }
            }
        }
    } else {
        message.channel.send(`:red_circle:  Vous ne m'avez pas configuré de préfixe. C'est que je ne dois pas être là depuis longtemps.`);
    }
}