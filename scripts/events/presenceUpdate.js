const poolQuery = require('./../functions/database/poolQuery');
const convertDate = require('./../functions/utils/convertDate');
const returnCityData = require('./../api/apixu/returnCityData');
module.exports = function(bot, oldMember, newMember) {
    const sendData = function(result) {
        if (result.last_day != new Date().getDate()) {
            returnCityData('reims').then(data => {
                newMember.user.send(`Bonjour **${newMember.user.username}** ! Nous sommes le **${convertDate(new Date.now(), '$n $d $o $y', 'fr')}** et il est **${convertDate(new Date.now(), '$h:$m', 'fr')}**, à **${data.location.name}**, il fait **${data.current.temp_c}** et le temps correspond à **${data.current.condition.text}**. Sur ce, bonne journée !`);
            }).catch(err => {
                newMember.user.send(`Salut **${newMember.user.username}** ! Nous sommes le **${convertDate(new Date.now(), '$n $d $o $y', 'fr')}** et il est **${convertDate(new Date.now(), '$h:$m', 'fr')}**. Malheureusement, je n'ai pas pu récupérer le bulletin météo pour aujourd'hui...`);
            })
        }
    }

    if (oldMember.user.presence.status == 'offline' && newMember.user.presence.status != 'offline') {
        if (newMember.user.presence.status == 'dnd') {
            poolQuery(`SELECT * FROM scheduled_meteo WHERE channelId=${newMember.user.id} AND type='dm' AND allowdnd=1`, function(err, result) {
                if (!err) {
                    sendData(result[0]);
                } else {
                    console.log(err);
                }
            })
        } else {
            poolQuery(`SELECT * FROM scheduled_meteo WHERE channelId=${newMember.user.id} AND type='dm'`, function(err, result) {
                if (!err) {
                    sendData(result[0]);
                } else {
                    console.log(err);
                }
            })
        }
    }
}