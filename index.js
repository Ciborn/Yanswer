const poolQuery = require('./scripts/functions/database/poolQuery.js');
const config = require('./config.json');
const Discord = require('discord.js');
const bot = new Discord.Client();
bot.login(config.app.token);

var guildPrefixes = new Map();
poolQuery(`SELECT * FROM guildssettings`, function(err, result) {
    if (err) {
        throw err;
    } else {
        for (var n = 0; n < Object.keys(result).length; n++) {
            guildPrefixes.set(result[n].guildId, result[n].prefix);
        }
    }
});

var guildSurnames = new Map();
poolQuery(`SELECT * FROM guildssettings`, function(err, result) {
    if (err) {
        throw err;
    } else {
        for (var n = 0; n < Object.keys(result).length; n++) {
            guildSurnames.set(result[n].guildId, result[n].surname);
        }
    }
});

bot.on('ready', () => {
    require('./scripts/events/ready')(bot);
});

bot.on('presenceUpdate', (oldMember, newMember) => {
    require('./scripts/events/presenceUpdate')(bot, oldMember, newMember);
})

bot.on('message', message => {
    if (message.author.id != config.app.clientID) {
        require('./scripts/events/message')(message, bot, guildPrefixes, guildSurnames, function(state, details, string) {
            if (state == 'handledError') {
                message.channel.send(`:red_circle:  Une erreur est survenue : ${string}`);
            }
        });
    }
});

bot.on('guildCreate', guild => {
    require('./scripts/events/guildCreate')(guild);
})